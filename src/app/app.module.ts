import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppareilComponent} from './appareil/appareil.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ListComponent} from './list/list.component';
import {HttpClientModule} from '@angular/common/http';
import {ListArticleComponent} from './list-article/list-article.component';
import {AddArticleComponent} from './add-article/add-article.component';
import {AddCategoryComponent} from './add-category/add-category.component';
import {ListCategoryComponent} from './list-category/list-category.component';
import {EditCategoryComponent} from './edit-category/edit-category.component';
import {NgbModalModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FirstComponent} from './first/first.component';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { DetailsArticleComponent } from './details-article/details-article.component';
import { DetailsCategoryComponent } from './details-category/details-category.component';
import { ListUsersComponent } from './User/list-users/list-users.component';
import {DataTablesModule} from 'angular-datatables';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'list', component: ListComponent},
  {path: 'listArticle', component: ListArticleComponent},
  {path: 'addArticle', component: AddArticleComponent},
  {path: 'addCategory', component: AddCategoryComponent},
  {path: 'listCategory', component: ListCategoryComponent},
  {path: 'detailsarticle', component: DetailsArticleComponent},
  {path: 'editarticle', component: EditArticleComponent},
  {path: 'detailsCategory', component: DetailsCategoryComponent},
  {path: 'listusers', component: ListUsersComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    AppareilComponent,
    HomeComponent,
    ListComponent,
    ListArticleComponent,
    AddArticleComponent,
    AddCategoryComponent,
    ListCategoryComponent,
    EditCategoryComponent,
    EditArticleComponent,
    DetailsArticleComponent,
    DetailsCategoryComponent,
    ListUsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    NgbModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DataTablesModule,
    NgbModalModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
