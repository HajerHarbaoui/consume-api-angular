import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Article} from '../Model/Article';


const API_URL = 'http://localhost:8000/api';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {
  }

  getAllArticles() {
    return this.http.get <Article[]>(API_URL + '/article');
  }

  getOneArticle(id) {
    return this.http.get <Article>(API_URL + '/article/' + id);
  }

  addProduct(json) {
    return this.http.post(API_URL + '/article', json);
  }

  deleteArticle(id: number) {
    return this.http.delete(API_URL + '/article/' + id);
  }

  updateArticle(article: Article) {
    const json = {
      'id': article.id,
      'nom': article.nom,
      'description': article.description,
      'quantite': article.quantite,
      'category': article.category,
    };
    return this.http.put(API_URL + '/article/' + article.id, json);
  }
}
