import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../Model/User';
import {Article} from '../Model/Article';

const API_URL = 'http://localhost:8000/api';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) {
  }

  getAllUsers() {
    return this.http.get <User[]>(API_URL + '/user');
  }
  getOneUser(id) {
    return this.http.get <User>(API_URL + '/user/' + id);
  }
  updateUser(user: User) {
    console.log(User);
    const json = {
      'id': user.id,
      'nom': user.nom,
      'prenom': user.prenom,
      'phoneNumber': user.phoneNumber,
      'mail': user.mail,
      'division': user.division,
    };
    console.log(json);
    return this.http.put(API_URL + '/user/' + user.id, json);
  }
}
