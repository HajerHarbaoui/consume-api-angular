import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Category} from '../Model/category';

const API_URL = 'http://localhost:8000/api';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  // @Output() public name = new EventEmitter();
  // @Output() public txt = new EventEmitter();


  constructor(private http: HttpClient) {
  }

  getAllCategories() {
    return this.http.get <Category[]>(API_URL + '/category');
  }

  addCategory(json) {
    return this.http.post(API_URL + '/category/', json);
  }

  getOneCategory(id) {
    return this.http.get <Category>(API_URL + '/category' + id);
  }

  deleteCategory(id: number) {
    return this.http.delete(API_URL + '/category/' + id);
  }

  updateCategory(category: Category) {
    console.log(category);
    const json = {
      'id': category.id,
      'name': category.name,
    };
    console.log(json);
    return this.http.put(API_URL + '/category/' + category.id, json);
  }
}

