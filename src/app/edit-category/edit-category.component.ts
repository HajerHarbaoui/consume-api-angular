import {Component, EventEmitter, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Category} from '../Model/category';
import {CategoryService} from '../Service/category.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  // providers: [ListCategoryComponent],
  providers: [CategoryService],
  // providers: [CategoryService, ListCategoryComponent],
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})

// @Injectable({
//   providedIn: ListCategoryComponent
// })

export class EditCategoryComponent implements OnInit {

  categoryForm: FormGroup;
  category: Category;
  lotId: number;
  updatedCategory: EventEmitter<Category> = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private categoryService: CategoryService,
              public modal: NgbActiveModal) {
  }

  ngOnInit() {
    console.log('category');
    console.log(this.category);
    console.log('category');
    // this.categoryForm.controls['id'].setValue(this.lotId);
    // this.categoryForm.patchValue({id: this.lotId});
    this.createForm();
    console.log('lotid');
    console.log(this.lotId);
    console.log('lotid');

  }

  createForm() {
    this.categoryForm = this.formBuilder.group({
      id: [this.lotId],
      name: [this.category.name, [Validators.required, Validators.minLength(5)]],
    });
  }

  updateCategory() {
    this.categoryService.updateCategory(this.categoryForm.value)
      .subscribe((category: Category) => {
        this.updatedCategory.emit(category);
      });
    this.modal.dismiss('Cross click');
  }
}
