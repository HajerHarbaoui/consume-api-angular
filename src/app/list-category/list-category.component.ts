import {Component, OnInit} from '@angular/core';
import {Category} from '../Model/category';
import {CategoryService} from '../Service/category.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EditCategoryComponent} from '../edit-category/edit-category.component';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.css'],
})
export class ListCategoryComponent implements OnInit {

  public categories: Category [] = [];
  public categoryId: number;
  public categoryName: string;

  constructor(private categoryService: CategoryService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.fetchAllCategories();
  }

  onDelete(id: number) {
    if (confirm('Are you sure to delete this category?')) {
      this.categoryService.deleteCategory(id).subscribe(res => {
        this.categoryService.getAllCategories().subscribe(data => {
          this.categories = data;
        });
      });
    }
  }

  updateCategory(Selectedcategory: Category, CategoryIndex: number) {
    // console.log(CategoryIndex);
    // console.log(Selectedcategory);
    const modal = this.modalService.open(EditCategoryComponent);
    modal.componentInstance.category = Selectedcategory;
    modal.componentInstance.lotId = CategoryIndex;

    console.log('************');
    console.log(modal.componentInstance.category);
    console.log('************');
    modal.componentInstance.updatedCategory.subscribe((updatedCategory: Category) => {
      this.categories[CategoryIndex] = updatedCategory;
      console.log(updatedCategory);
      this.fetchAllCategories();
    });
  }

  DetailsCategory(selecrtedCategory: number, nameCategory: string) {
    // console.log(selecrtedCategory);
    // this.categoryService.name.emit(selecrtedCategory.toString());
    this.categoryName = nameCategory;
    this.categoryId = selecrtedCategory;
    // selecrtedCategory = null;
  }

  private fetchAllCategories() {
    this.categoryService.getAllCategories().subscribe((data: Category[]) => {
      this.categories = data;
      console.log(this.categories);

    });
  }
}
