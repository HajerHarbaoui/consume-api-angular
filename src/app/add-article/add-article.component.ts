import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../Model/Article';
import {ProductService} from '../Service/product.service';
import {CategoryService} from '../Service/category.service';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {

  article: Article = new Article();
  articleForm: FormGroup;
  categories: any[] = [];

  constructor(private formBuilder: FormBuilder,
              private articleService: ProductService,
              private categoryService: CategoryService) {
  }

  ngOnInit() {
    this.categoryService.getAllCategories().subscribe(data => {
      this.categories = data;
      console.log(this.categories);
    });

    this.createFormAddArticle();
  }

  createFormAddArticle() {
    this.articleForm = this.formBuilder.group({
      Nom: [''],
      description: [''],
      Quantite: [],
      category: ['', [Validators.required]],
    });
  }

  onSubmit() {
    console.log(this.articleForm.value);
    this.articleService.addProduct(this.articleForm.value)
      .subscribe(data => {
        console.log((data));
      });
  }

}
