import {Component, EventEmitter, OnInit} from '@angular/core';
import {ProductService} from '../Service/product.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CategoryService} from '../Service/category.service';
import {Article} from '../Model/Article';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {

  public article: Article = new Article();
  editArticleForm: FormGroup = new FormGroup({
    nom: new FormControl(),
    description: new FormControl(),
    quantite: new FormControl(),
    category: new FormControl(),
  });
  categories: any[] = [];
  public idArticleToUpdate = localStorage.getItem('idArticleToUpdate');
  public categoryId: number;

  constructor(private articleService: ProductService,
              private categoryService: CategoryService,
              private formBuilder: FormBuilder,
              private router: Router
  ) {
  }

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe(data => {
      this.categories = data;
      console.log(this.categories);
    });
    this.createForm();
  }

  createForm() {
    this.articleService.getOneArticle(this.idArticleToUpdate).subscribe((data: Article) => {
      this.article = data;
      this.categoryId = this.article.category.id;
      console.log(this.categoryId);
      this.editArticleForm = this.formBuilder.group({
        id: [this.article.id],
        nom: [this.article.nom, [Validators.required, Validators.minLength(5)]],
        description: [this.article.description],
        quantite: [this.article.quantite],
        category: [this.categoryId],
      });
    });
  }

  onSubmit() {
    this.articleService.updateArticle(this.editArticleForm.value)
      .subscribe((article: Article) => {
        this.router.navigate(['/listArticle']);
        localStorage.removeItem('idArticleToUpdate');
      });
  }
}
