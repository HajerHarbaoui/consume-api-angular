import {Component, Input, OnInit, Output} from '@angular/core';
import {CategoryService} from '../Service/category.service';

@Component({
  selector: 'app-details-category',
  templateUrl: './details-category.component.html',
  styleUrls: ['./details-category.component.css'],
})
export class DetailsCategoryComponent implements OnInit {

  @Input() childExemple: number;
  @Input() childExempletwo: string;

  constructor(private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    // this.categoryService.name.subscribe(name => {
    //   console.log('name');
    //   console.log(name);
    //   console.log('name');
    //   this.childExemple = name;
    //
    // });
  }

}
