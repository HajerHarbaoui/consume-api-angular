import {Category} from './category';

export class Article {
  id: number;
  nom: string;
  description: string;
  quantite: number;
  public category: Category;
}
