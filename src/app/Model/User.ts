export class User {
  id: number;
  nom: string;
  prenom: string;
  mail: string;
  password: string;
  accountType: string;
  etatCompte: string;
  phoneNumber: string;
  codeFournisseur: string;
  libelleFournisseur: string;
  libelleSagem: string;
  division: string;
  dateCreation: Date;
}
