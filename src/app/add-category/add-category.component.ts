import {Component, OnInit} from '@angular/core';
import {Category} from '../Model/category';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {CategoryService} from '../Service/category.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  category: Category = new Category();
  categoryForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private categoryService: CategoryService) {
  }

  ngOnInit() {
    this.categoryForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  onSubmit() {
    console.log(this.categoryForm.value);
    this.categoryService.addCategory(this.categoryForm.value)
      .subscribe(data => {
        console.log((data));
      });
  }
}
