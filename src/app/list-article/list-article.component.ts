import {Component, OnInit} from '@angular/core';
import {ProductService} from '../Service/product.service';
import {Article} from '../Model/Article';
import {EditArticleComponent} from '../edit-article/edit-article.component';

@Component({
  providers: [EditArticleComponent],
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.css']
})
export class ListArticleComponent implements OnInit {

  public articles: Article [] = [];
  public article: Article;


  constructor(private articleService: ProductService) {
  }

  ngOnInit() {
    this.fecthAllArticle();
  }

  onDeleteArticle(id: number) {
    if (confirm('Are you sure to delete this Article?')) {
      this.articleService.deleteArticle(id).subscribe(res => {
        this.articleService.getAllArticles().subscribe(data => {
          this.articles = data;
        });
      });
    }
  }

  DetailsArticle(SelectedArticle: number) {
    localStorage.setItem('key', SelectedArticle.toString());
    console.log(localStorage.getItem('key'));
  }

  onUpdateArticle(article: Article, selectedArticle: number) {
    localStorage.setItem('idArticleToUpdate', selectedArticle.toString());
    console.log(localStorage.getItem('idArticleToUpdate'));
  }

  private fecthAllArticle() {
    this.articleService.getAllArticles().subscribe((data: Article[]) => {
      this.articles = data;
      console.log(this.articles);
    });
  }
}
