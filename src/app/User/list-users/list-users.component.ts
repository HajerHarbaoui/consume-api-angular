import {Component, OnInit, ViewChild, AfterViewInit, OnDestroy} from '@angular/core';
import {UserServiceService} from '../../Service/user-service.service';
import {User} from '../../Model/User';
import {Subject} from 'rxjs';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})

export class ListUsersComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild (DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  public users: User [] = [];
  dtTrigger: Subject<User> = new Subject();
  closeResult = '';
  // idUserToUpdate = 1;
  public updatedUser:User;
  editUserForm: FormGroup = new FormGroup({
    nom: new FormControl(),
    prenom: new FormControl(),
    mail: new FormControl(),
    phoneNumber: new FormControl(),
    division: new FormControl(),
  });

  constructor(private userService: UserServiceService ,
              private formBuilder: FormBuilder ,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
    this.fecthAllusers();
  }
  // **************************users list***************************
  fecthAllusers() {
    this.userService.getAllUsers().subscribe((
      users) => {
      this.users = users;
      this.rerender();
      console.log(this.users);
    });
  }
  // **************************users list***************************

  // **************************Modal***************************
  open(content, idUserToUpdate: number) {
    this.userService.getOneUser(idUserToUpdate).subscribe((data: User) => {
      this.updatedUser = data;
      console.log(this.updatedUser);
      this.editUserForm = this.formBuilder.group({
        id: [this.updatedUser.id],
        nom: [this.updatedUser.nom, [Validators.required, Validators.minLength(5)]],
        prenom: [this.updatedUser.prenom],
        mail: [this.updatedUser.mail],
        phoneNumber: [this.updatedUser.phoneNumber],
        division: [this.updatedUser.division],
      });
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log('test');
      this.onSubmit();
      this.fecthAllusers();

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // **************************Modal**************************

  // **************************create form (EDIT)***************************
  onSubmit() {
    this.userService.updateUser(this.editUserForm.value)
      .subscribe((user: User) => {
      console.log('done');
      });
  }
  // **************************refresh datatable***************************
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  // **************************refresh datatable***************************

}
