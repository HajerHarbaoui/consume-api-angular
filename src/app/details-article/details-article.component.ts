import {Component, OnInit} from '@angular/core';
import {Article} from '../Model/Article';
import {ProductService} from '../Service/product.service';
import {Category} from '../Model/category';

@Component({
  selector: 'app-details-article',
  templateUrl: './details-article.component.html',
  styleUrls: ['./details-article.component.css']
})
export class DetailsArticleComponent implements OnInit {

  public article: Article = new Article();
  public category: Category = new Category();
  public findbById = localStorage.getItem('key');

  constructor(private articleService: ProductService) {
  }

  ngOnInit(): void {
    this.articleService.getOneArticle(this.findbById).subscribe((data: Article) => {
      this.article = data;
      this.category.name = this.article.category.name.concat('');
      // console.log(data.category.name.concat(''));
    });
  }

}
